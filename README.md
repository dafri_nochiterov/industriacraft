# WE'RE MOVING TO GITLAB
# [Visit new repository](https://gitlab.com/DaFri-Nochiterov/industriacraft)



## About

IndustriaCraft it's client of Minecraft game with some mods for realistic, industrial life in your MC world! Explore mine, eat meat, chop wood, drink milk, do hard work, do experiments. Play alone, or with players. Have fun! 

## Installation 

First what you need is download files.
You can download archive in [downloads section][downloads_url]

#### Or by this links:

* [Not compressed](https://bitbucket.org/dafri_nochiterov/industriacraft/get/master.zip)
* [Compressed](https://bitbucket.org/dafri_nochiterov/industriacraft/get/master.tar.gz)
* [Hard compressed](https://bitbucket.org/dafri_nochiterov/industriacraft/get/master.tar.bz2)

[downloads_url]: https://bitbucket.org/dafri_nochiterov/industriacraft/downloads#branch-downloads

Okay, you download this files, what to do after that?

You can download any launcher, but I very recommend to TLauncher. 
TLauncher is beauty, simple launcher 

## Mods in this client

Thanks you, authors of mods, you're cool guys.

| Mod name                               | Version                |
|----------------------------------------|:----------------------:|
| Advanced Solar Panels                  | 3.5.1                  |
| AppliedEnergistics2                    | rv-2-stable-10         |
| ArmorStatusHUD                     (cu)| 1.28                   |
| Better Foliage                         | 1.0.15                 |
| BetterFonts                            | 1.1.3                  |
| BiblioCraft                            | 1.11.4                 |
| Biomes O' Plenty                       | 2.1.0                  |
| bspkrsCore                             | 6.15                   |
| Build Mod                              | `N/A`                  |
| BuildCraft                             | 7.1.14                 |
| Carpenter's Blocks                     | 3.2.5                  |
| Chicken Shed                           | 1.1.3                  |
| Copious Dogs     (ptr)                 | 1.1.2                  |
| CoroAI                                 | `N/A`                  |
| CraftGuide                             | 1.6.8.1                |
| Damage Indicators (cu)                 | 3.2.0                  |
| DrZhark's CustomSpawner                | 3.3.0                  |
| Dynamic Lights                         | `N/A`                  |
| Extended Mod Config                    | `N/A`                  |
| Extended Rendered                      | `N/A`                  |
| ExtrabiomesXL     (cu)                 | 3.16.alpha             |
| Forestry                               | 4.2.0.47               |
| Furnace 3D                             | 1.2                    |
| Galacticraft Core                      | 3.0.12                 |
| Galacticraft Planets                   | 3.0.12                 |
| Gases Framework                        | 1.1.2                  |
| Glenn's Gases                          | 1.6.2                  |
| Graviation Suite                       | 2.0.3                  |
| Horizontal Glass Panes                 | `N/A`                  |
| iChunUtil        (nu)                  | 4.0.0                  |
| Industrial Craft 2                     | 2.2.796-experimental   |
| Inventory Tweaks (cu)                  | 1.58-147-645ca10       |
| Iron Chest       (cu)                  | 6.0.41 build 729       |
| JACB                                   | `N/A`                  |
| Lots of Food                           | 1.7.10 (pi)            |
| Mantle                                 | 0.3.2b                 |
| Micdoodle8 Core                        | `N/A`                  |
| MrCrayfish's Furniture Mod     (cu)    | 3.4.7                  |
| MrTJPCore                              | 1.0.5.11               |
| NEI Addons + Botany                    | 1.12.5.17              |
| NEI Integration                        | 1.0.13                 |
| Not Enought Items                      | 1.0.4.95               |
| Nuclear Control                        | `N/A`                  |
| Optifine                               | HD_U_C1                |
| Player API 1.2                         | 1.2                    |
| ProjectRed                      (cu)   | 4.5.10.61              |
| Quick Hotbar                           | 1.01                   |
| Railcraft                              | 9.8.0.0                |
| Realistic Terrain Generation           | 0.1.0                  |
| ScoutCraft                    (cu+prt) | beta 1.2.0             |
| ShadersModCore                         | `N/A`                  |
| Smart Core                             | `N/A`                  |
| Smart Moving                           | 15.1                   |
| Smart Render                           | 2.0                    |
| Somnia                                 | 1.4.7.53               |
| SomniaCore                             | 1.0                    |
| Sound Filters                          | 0.7                    |
| Tinkers' Construct                     | 1.8.8                  |
| Treecapitator                  (cu)    | 2.0.4                  |
| UsefulFood                     (cu)    | 1.5.0                  |
| Waila                                  | 1.5.10                 |

defines: 
* cu (check updated) - needs to check new version of this mod

* nu (need update) - needs to download new version of this mod

* ptr (plans to remove) - this mod can be deleted in next versions

* pi (possibly invalid) - this mod version can be invalid 

